name := "Cassandra Hadoop filesystem"

normalizedName := "cassandra-hdfs"

organization := "com.filez.hadoop"

organizationName := "Filez.com"

startYear := Some(2020)

version := "0.0.1"

crossScalaVersions := Seq("2.12.11", "2.11.12", "2.13.3")

ThisBuild / scalaVersion := crossScalaVersions.value.head

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"

libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-core" % "3.9.0"

// synced with spark 2.6 dependencies
libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "2.6.5"

