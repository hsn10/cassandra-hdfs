package com.filez.hadoop.cassandra.hdfs

import java.net.URI
import org.apache.hadoop.conf.Configuration

import com.datastax.driver.core.Cluster

/* extending AbstractFS:
https://github.com/apache/hadoop/blob/branch-2.6/hadoop-common-project/hadoop-common/src/main/java/org/apache/hadoop/fs/AbstractFileSystem.java
*/

/**
 Create cassandra backed Hadoop compatible filesystem
 @param uri cassandra://host:[port]/keyspace
 @param conf Hadoop configuration
*/

class CassandraHDFS(uri: URI, conf: Configuration) extends org.apache.hadoop.fs.AbstractFileSystem(
         uri, "cassandra", false, 9042)
{
   val cluster = { var b = Cluster.builder().addContactPoint(uri.getHost())
                   if(uri.getPort()>0) b = b.withPort(uri.getPort())
                   b.withoutMetrics().withoutJMXReporting().build()
                 }
   val session = cluster.connect(
   if(uri.getPath().startsWith("/")) uri.getPath().substring(1) else "hdfs"
   ).init()

   override
   def getUriDefaultPort() = 9042
}